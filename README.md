# Docker Workshop - Windows
Lab 05: Working with Docker Hub (Registry)

---

## Preparations

 - Create a new folder for the lab:
```
$ mkdir lab-05
$ cd lab-05
```



## Instructions

 - Browse to docker hub and sign up:
```
https://hub.docker.com
```

 - Create a new repository called "win-voting-app":
```
https://hub.docker.com/add/repository/
```

 - Create a index.html file with the content below:
```
<!DOCTYPE html>
<html>
<body>
<h2>Vote!</h2>
<form>
  <input type="radio" name="candidate" value="David" checked>David<br>
  <input type="radio" name="candidate" value="Moises">Moises<br>
  <input type="radio" name="candidate" value="Daniel">Daniel<br>  
</form> 
<button>Submit</button> 
</body>
</html>
```

 - Create a Dockerfile with the content below:
```
FROM mcr.microsoft.com/windows/servercore:ltsc2019

RUN powershell -Command \
    Add-WindowsFeature Web-Server; \
    Invoke-WebRequest -UseBasicParsing -Uri "https://dotnetbinaries.blob.core.windows.net/servicemonitor/2.0.1.6/ServiceMonitor.exe" -OutFile "C:\ServiceMonitor.exe"

COPY index.html "C:\\inetpub\\wwwroot"

EXPOSE 80

ENTRYPOINT ["C:\\ServiceMonitor.exe", "w3svc"]
```

 - Build the docker image::
```
$ docker build -t win-voting-app:latest .
```

 - Run the application to ensure that everything it's ok:
```
$  docker run -d -p 9090:80 --name win-voting-app win-voting-app:latest
```

 - Browse to the application page:
```
http://server-ip:9090
```

 - Remove the created container:
```
$ docker rm -f win-voting-app
```

 - Let's create two tags for the image we want to push to docker hub:
```
$ docker tag win-voting-app:latest <your-dockerhub-username>/win-voting-app:1.0
$ docker tag win-voting-app:latest <your-dockerhub-username>/win-voting-app:latest
```

 - Login to docker hub from the terminal:
```
$ docker login
Login with your Docker ID to push and pull images from Docker Hub. If you don't have a Docker ID, head over to https://hub.docker.com to create one.
Username (): <your-dockerhub-username>
Password: <your-dockerhub-password>
```

 - Push the images to docker hub:
```
$ docker push <your-dockerhub-username>/win-voting-app:1.0
$ docker push <your-dockerhub-username>/win-voting-app:latest
```

 - See the pushed images in the docker hub page
